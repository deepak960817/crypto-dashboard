import { actionTypes } from "../actions/actionTypes";

const initialState = {
    cryptoItem: [],
    searchText: ""
}

export const cryptoReducer = (state = initialState, {type, payload }) => {
    
    switch(type)
    {
        case actionTypes.SET_CRYPTO:
            return {
                ...state,
                cryptoItem: payload
            }

        case actionTypes.SET_SEARCH:
            return {
                ...state,
                searchText: payload
            }

        default:
            return state;
    }
}