import { createStore } from "redux";
import { cryptoReducer } from "./reducers/cryptoReducer";

export default createStore(cryptoReducer, {});