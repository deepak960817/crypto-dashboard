import { actionTypes } from "./actionTypes";

export const setCrypto = (crypto) => {
    return {
        type: actionTypes.SET_CRYPTO,
        payload: crypto
    }
}

export const setSearch = (text) => {
    return {
        type: actionTypes.SET_SEARCH,
        payload: text
    }
}