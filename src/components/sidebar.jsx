import React, {useRef} from "react";
import { Card } from "react-bootstrap";
import { Form } from "react-bootstrap";
import {Link} from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux";
import cryptos from '../data/cryptos.json'
import {setCrypto, setSearch} from "../redux/actions/actions";

const SideBar = () => {

    const dispatch = useDispatch();
    const cryptoSearched = useSelector((state) => state.searchText)

    const handleChange = (e) => {
        e.preventDefault();
        dispatch(setSearch(e.target.value))
    }

    let searchedCryptos;
    let refrencevalue = useRef();
    
    if(cryptoSearched === "" || cryptoSearched === undefined)
    {
        searchedCryptos = cryptos;
    }
    else
    {
        searchedCryptos = cryptos.filter((item) => {
            console.log(refrencevalue.current)
            return (item.name.toLowerCase().includes(refrencevalue.current.value.toLowerCase()));
        })
    }
    
    return (
        <div className="outerbox" style={{ height: '80vh', width: '20vw', backgroundColor: 'white', border: '1px solid lightgray', borderRadius:'0.3rem', position: 'fixed', overflowY: 'scroll', top: '10vh', bottom: '10vh', zIndex: '1' }}>
            <div style={{display:'flex'}}>
            <Link to={"/"} style={{textDecoration:'none', color:'black'}}>
                <p style={{fontWeight: 'bold', margin:'1rem 0 0 1rem'}}>{` < `}</p>
            </Link>
            <p style={{textAlign:'left', marginLeft:'0.5rem'}}>Cryptos({searchedCryptos.length})</p>
            </div>
            <Form>
                <Form.Group className="mb3" style={{ margin: '0 0 1rem 1rem' }}>
                    <Form.Control type="text" placeholder=' Search' ref={refrencevalue} style={{ width:'90%', height:'1.5rem', border: '0.5px solid gray' }} onChange={(e) => handleChange(e)}/>
                </Form.Group>
            </Form>
            <hr style={{ width: '90%', color:'whitesmoke' }}></hr>
            {
                searchedCryptos.map((item) => {
                    return (
                        <div style={{ width: '18rem' }} key={item.symbol}>
                            <Link to={`/${item.symbol}`} key={item.symbol} style={{textDecoration: 'none'}}>
                            <Card style={{ width: '16rem', height: '3.5rem', display: 'flex' }} onClick={() => dispatch(setCrypto(item))}>
                                <Card.Img variant="top" src={item.img} height="20rem" width="20rem" style={{ margin: '0.5rem 0rem 0rem 2rem' }} />
                                <Card.Body style={{ display: 'flex', flexDirection: 'column', alignItems: 'start', margin: '0.4rem 0rem 0rem 1rem' }}>
                                    <Card.Title style={{ fontSize: '0.9rem', color: 'black', margin: '0 0 -0.5rem 0'  }}>{item.symbol}</Card.Title>
                                    <Card.Text style={{ color: 'gray', fontSize: '0.8rem' }}>{item.name}</Card.Text>
                                </Card.Body>
                            </Card>
                            </Link>
                            <hr style={{ width: '90%', color:'whitesmoke' }}></hr>
                        </div>
                    )

                })
            }
        </div>
    )
}

export default SideBar;