import React from 'react';
import { Card } from "react-bootstrap";
import greenTriangle from '../data/triangle-xxl.png'
import redTriangle from '../data/redTriangle.png'

const CryptoPage = ({prop}) => {
    return (
        <div style={{ height: '80vh', width: '70vw', marginLeft: '20vw', border: '1px solid lightgray', backgroundColor: 'white', position: 'fixed', overflowY: 'scroll', top: '10vh', bottom: '10vh', zIndex: '1' }}>
            <span style={{ display: 'flex' }}>
                <img src='https://cryptogeek.info/images/logo-cryptogeek-circle.svg' alt='logo' height="30rem" width="30rem" style={{ margin: '1rem 0.5rem 0 1rem' }}></img>
                <h3 style={{ textAlign: 'left' }}>My Cryptos</h3>
            </span>
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <Card style={{ display: 'flex', width: '50%', margin: '1rem 1.1rem 1rem 1.1rem', border: '1px solid lightgray', borderRadius: '0.4rem', backgroundColor: 'whitesmoke' }}>
                    <Card.Img variant="left" src={prop.img} height="100rem" width="100rem" style={{ margin: '1.5rem 2rem 0rem 1.5rem', border: '0.5px solid lightgray', borderRadius: '50%' }} />
                    <Card.Body style={{ display: 'flex', flexDirection: 'column', alignItems: 'start', margin: '1.5rem 0rem 0rem 1.5rem' }}>
                        <Card.Title style={{ fontSize: '3rem', fontWeight: 'bolder' }}>{prop.name}</Card.Title>
                        <Card.Text style={{ color: 'gray', fontSize: '1.5rem', margin: '0.3rem 0rem 0rem 0rem' }}>{prop.symbol}</Card.Text>
                        <div style={{ display: 'flex', alignItems: 'end', justifyContent: 'start' }}>
                            <Card.Text style={{ color: prop.value > 0 ? 'green' : 'red', fontSize: '1.3rem', fontWeight:'1500' }}>{prop.value}</Card.Text>
                            <Card.Text style={{ color: prop.percnt_change > 0 ? 'green' : 'red', fontSize: '0.8rem', margin: '0 0.2rem 1.5rem 0.3rem' }}>({prop.percnt_change})</Card.Text>
                            <img src={prop.percnt_change > 0 ? greenTriangle : redTriangle} alt='logo' height='10' width='10' style={{ marginBottom: '1.6rem' }}></img>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'flex-start' }}>
                            <Card.Text style={{ color: 'gray', fontSize: '0.8rem', marginTop: '-1.2rem' }}>CHANGE</Card.Text>
                            <hr style={{ width: '11rem', marginTop: '-0.2rem' }}></hr>
                        </div>
                        <div style={{ display: 'flex' }}>
                            <span style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
                                <Card.Text style={{ fontSize: '1rem', margin: '0 1rem 0 0' }}>{prop.buy}</Card.Text>
                                <p style={{ fontSize: '0.75rem', margin: '0.2rem 0 0 0', color: 'gray' }}>Buy</p>
                            </span>
                            <hr style={{ height: '1.5rem' }}></hr>
                            <span style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
                                <Card.Text style={{ fontSize: '1rem', margin: '0 0 0 1rem' }}>{prop.sell}</Card.Text>
                                <p style={{ fontSize: '0.75rem', margin: '0.2rem 0 0 1rem', color: 'gray' }}>Sell</p>
                            </span>
                        </div>
                        <hr style={{ width: '100%', marginTop: '0.5rem' }}></hr>
                        <div style={{ display: 'flex' }}>
                            <span style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
                                <Card.Text style={{ fontSize: '1rem', margin: '0 1rem 0 0' }}>{prop.askPrice}</Card.Text>
                                <p style={{ fontSize: '0.75rem', margin: '0.2rem 0 0 0', color: 'gray' }}>Ask Price</p>
                            </span>
                            <hr style={{ height: '1.5rem' }}></hr>
                            <span style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
                                <Card.Text style={{ fontSize: '1rem', margin: '0 0 0 1rem' }}>{prop.bidPrice}</Card.Text>
                                <p style={{ fontSize: '0.75rem', margin: '0.2rem 0 0 1rem', color: 'gray' }}>Bid Price</p>
                            </span>
                            <hr style={{ height: '1.5rem', margin:'0 0 0 1rem'}}></hr>
                            <span style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
                                <Card.Text style={{ fontSize: '1rem', margin: '0 0 0 1rem' }}>{prop.volume}</Card.Text>
                                <p style={{ fontSize: '0.75rem', margin: '0.2rem 0 0 1rem', color: 'gray' }}>Volume</p>
                            </span>
                        </div>
                        <hr style={{ width: '100%', marginTop: '0.5rem' }}></hr>
                        <div style={{ display: 'flex' }}>
                            <span style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
                                <Card.Text style={{ fontSize: '1rem', margin: '0 1rem 0 0' }}>{prop.lowPrice}</Card.Text>
                                <p style={{ fontSize: '0.75rem', margin: '0.2rem 0 2rem 0', color: 'gray' }}>Lowest Price</p>
                            </span>
                            <hr style={{ height: '1.5rem' }}></hr>
                            <span style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
                                <Card.Text style={{ fontSize: '1rem', margin: '0 0 0 1rem' }}>{prop.highPrice}</Card.Text>
                                <p style={{ fontSize: '0.75rem', margin: '0.2rem 0 2rem 1rem', color: 'gray' }}>Highest Price</p>
                            </span>
                            <hr style={{ height: '1.5rem', margin:'0 0 0 0.5rem' }}></hr>
                            <span style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
                                <Card.Text style={{ fontSize: '1rem', margin: '0 0 0 1rem' }}>{prop.lastPrice}</Card.Text>
                                <p style={{ fontSize: '0.75rem', margin: '0.2rem 0 2rem 1rem', color: 'gray' }}>Last Price</p>
                            </span>
                        </div>
                    </Card.Body>
                </Card>
            </div>
        </div>
    )
}

export default CryptoPage;