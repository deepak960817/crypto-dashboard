import React from 'react';
import { Card } from "react-bootstrap";
import {Link} from 'react-router-dom';
import { useDispatch } from "react-redux";
import {setCrypto} from '../redux/actions/actions';
import cryptos from '../data/cryptos.json'
import greenTriangle from '../data/triangle-xxl.png'
import redTriangle from '../data/redTriangle.png'

const HomePage = () => {

    const dispatch = useDispatch();

    return (
        <div style={{ height: '80vh', width: '70vw', marginLeft: '20vw', border: '1px solid lightgray', borderRadius:'0.3rem', backgroundColor: 'white', position: 'fixed', overflowY: 'scroll', top: '10vh', bottom: '10vh', zIndex: '1' }}>
            <span style={{ display: 'flex' }}>
                <img src='https://cryptogeek.info/images/logo-cryptogeek-circle.svg' alt='logo' height="30rem" width="30rem" style={{ margin: '1rem 0.5rem 0 1rem' }}></img>
                <h3 style={{ textAlign: 'left' }}>My Cryptos</h3>
            </span>
            <div style={{ display: 'flex', justifyContent: 'flex-start', flexWrap: 'wrap', marginTop:'-0.5rem' }}>
                {
                    cryptos.map((item) => {
                        return (
                            <div key={item.symbol}>
                                <Link to={`/${item.symbol}`} key={item.symbol} style={{textDecoration: 'none', color:'black'}}>
                                <Card style={{ display: 'flex', width: '18.5rem', height: '13rem', margin: '1rem 1.1rem 1rem 1.1rem', border: '1px solid lightgray', borderRadius: '0.4rem', backgroundColor: 'whitesmoke' }} onClick={() => dispatch(setCrypto(item))} >
                                    <Card.Img variant="left" src={item.img} height="40rem" width="40rem" style={{ margin: '1.5rem 0rem 0rem 1.5rem', border: '0.5px solid lightgray', borderRadius: '50%' }} />
                                    <Card.Body style={{ display: 'flex', flexDirection: 'column', alignItems: 'start', margin: '1.5rem 0rem 0rem 1.5rem' }}>
                                        <Card.Title style={{ fontSize: '1.2rem', fontWeight: 'bolder' }}>{item.name}</Card.Title>
                                        <Card.Text style={{ color: 'gray', fontSize: '0.8rem', margin: '0.3rem 0rem 0rem 0rem' }}>{item.symbol}</Card.Text>
                                        <div style={{ display: 'flex', alignItems: 'end', justifyContent: 'start' }}>
                                            <Card.Text style={{ color: item.value > 0 ? 'green' : 'red', fontSize: '1.3rem', fontWeight:'bold' }}>{item.value}</Card.Text>
                                            <Card.Text style={{ color: item.percnt_change > 0 ? 'green' : 'red', fontSize: '0.8rem', margin: '0 0.2rem 1.5rem 0.3rem' }}>({item.percnt_change})</Card.Text>
                                            <img src={item.percnt_change > 0 ? greenTriangle : redTriangle} alt='logo' height='10' width='10' style={{ marginBottom: '1.6rem' }}></img>
                                        </div>
                                        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'flex-start' }}>
                                            <Card.Text style={{ color: 'gray', fontSize: '0.8rem', marginTop: '-1.2rem' }}>CHANGE</Card.Text>
                                            <hr style={{ width: '11rem', marginTop: '-0.2rem', color:'lightgray' }}></hr>
                                        </div>
                                        <div style={{ display: 'flex' }}>
                                            <span style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
                                                <Card.Text style={{ fontSize: '1rem', margin: '0 1rem 0 0' }}>{item.buy}</Card.Text>
                                                <p style={{ fontSize: '0.75rem', margin: '0.2rem 0 0 0', color: 'gray' }}>Buy</p>
                                            </span>
                                            <hr style={{ height: '1.5rem', color:'lightgray', }}></hr>
                                            <span style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
                                                <Card.Text style={{ fontSize: '1rem', margin: '0 0 0 1rem' }}>{item.sell}</Card.Text>
                                                <p style={{ fontSize: '0.75rem', margin: '0.2rem 0 0 1rem', color: 'gray' }}>Sell</p>
                                            </span>
                                        </div>
                                    </Card.Body>
                                </Card>
                                </Link>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default HomePage;