import React, { Fragment } from 'react';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { useSelector } from 'react-redux';
import HomePage from '../src/components/homepage';
import SideBar from '../src/components/sidebar';
import CryptoPage from '../src/components/cryptopage';
import './App.css';

const App = () => {

  const crypto = useSelector((state) => state.cryptoItem);

  return (
    <Fragment>
    <Router>
      <Routes>
        <Route exact path={'/'} element={
         <div style={{ margin: '10vh 5vw 10vh 5vw', display: 'flex', justifyContent: 'start', backgroundColor: 'white', border: '1px solid lightgray', borderRadius:'0.3rem', }}>
           <SideBar />
           <HomePage/>
         </div>
        } />
        {
          crypto !== undefined ?
          <Route exact path={`/${crypto.symbol}`} element={
            <div style={{ margin: '10vh 5vw 10vh 5vw', display: 'flex', justifyContent: 'start', backgroundColor: 'white', border: '1px solid lightgray', borderRadius:'0.3rem', }}>
              <SideBar />
              <CryptoPage prop={crypto}/>
            </div>
           } />
           :
           <></>
        }
        
      </Routes>
    </Router>
  </Fragment>
  );
}

export default App;